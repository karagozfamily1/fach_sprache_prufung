import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'mobile_screen_home.dart';
import 'controllers/controller_translations.dart';

final tag = "main.dart";

void main() async {
  //Programın sadece dikey konumda çalışması için alttaki kodlar yazılıyor. down yazan yeri silince yine aynı şekilde çalışıyor.....
  await GetStorage.init();

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(MyApp());
  print("$tag dosyası main fonksiyonu çalıştı");
}

class MyApp extends StatelessWidget {
  MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        translations: TranslationService(),
        // your translations
        locale: Locale('en', 'US'),
        // translations will be displayed in that locale
        fallbackLocale: Locale('en', 'US'),
        // specify the fallback locale in case an invalid locale is selected.

        debugShowCheckedModeBanner: false,
        title: "Fach Sprache Prüfung".tr,
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        home: MobileScreenHome());
  }
}
