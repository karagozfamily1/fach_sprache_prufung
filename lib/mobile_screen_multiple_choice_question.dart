import 'package:fach_sprache_prufung/mobile_screen_navigation_drawer.dart';
import 'package:dropdown_button2/custom_dropdown_button2.dart';
import 'package:fach_sprache_prufung/controllers/controller_multiple_questions.dart';
import 'package:fach_sprache_prufung/mobile_screen_working_options.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'controllers/controller_get_storage.dart';
import 'controllers/controller_general.dart';
import 'mobile_screen_home.dart';

class MobileScreenMultipleChoiceQuestion extends StatelessWidget {
  MobileScreenMultipleChoiceQuestion({Key key}) : super(key: key);

  final generalController = Get.put(GeneralController());
  final multipleChoiceQuestionController = Get.put(MultipleQuestionController());
  final getStorageController = Get.put(GetStorageController());
  final tag = "mobile_screen_multiple_choice_question.dart";

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Get.to(() => MobileScreenNeumorphicWorkingOptions());
      },
      child: SafeArea(
        child: Scaffold(
          drawer: NavigationDrawer(),
          appBar: AppBar(iconTheme: IconThemeData(color: Colors.black),
            toolbarHeight: 60,
            title: Text(
              "Multiple Choice Questions".tr,
              style: TextStyle(color: Colors.black),
            ),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black54, width: 2),
                // borderRadius: BorderRadius.only(
                //   topLeft: Radius.circular(20),
                //   bottomRight: Radius.circular(20),
                // ),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF4da7db),
                    Colors.white,
                  ],
                ),
              ),
            ),
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      Get.offAll(MobileScreenHome());
                      generalController.hinttext.value = "Choose".tr;
                    },
                    child: Icon(Icons.home, color: Colors.black),
                  )),
            ],
          ),
          body: buildBody(),
        ),
      ),
    );
  }

  buildBody() {
    var liste = generalController.word_list_of_choosen_bundesland;

    if (liste.length < 20) {
      return Scaffold(
        body: Container(decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Colors.red, Colors.lightBlue],
            ),
          ),
          child: Center(child: Text("There aren't enough words in your list (min 20)".tr)),
        ),
      );
    } else {
      return Obx(
        () => buildBody1(),
      );
    }
  }

  buildBody1() {
    var index = multipleChoiceQuestionController.index.value;
    var savedQuestionNumberValue = getStorageController.savedQuestionNumber.value;
    final savedQuestionNumberStorage = GetStorage();
    savedQuestionNumberStorage.writeIfNull("savedQuestionNumberKey", savedQuestionNumberValue);

    if (savedQuestionNumberStorage.read('savedQuestionNumberKey') != null) {
      savedQuestionNumberValue = savedQuestionNumberStorage.read('savedQuestionNumberKey');
    }

    print("kelime listesi uzunluğu ${multipleChoiceQuestionController.kelime_listesi.length}");
    List indexabcd_1_artan = [
      generalController.index,
      generalController.index + 2,
      generalController.index + 4,
      generalController.index + 6,
    ];
    List indexabcd_2_artan = [
      generalController.index + 2,
      generalController.index + 6,
      generalController.index,
      generalController.index + 4,
    ];
    List indexabcd_3_artan = [
      generalController.index + 6,
      generalController.index + 4,
      generalController.index + 2,
      generalController.index,
    ];
    List indexabcd_4_artan = [
      generalController.index + 4,
      generalController.index,
      generalController.index + 6,
      generalController.index + 2,
    ];
    List indexabcd_1_azalan = [
      generalController.index - 1,
      generalController.index,
      generalController.index - 3,
      generalController.index - 5,
    ];
    List indexabcd_2_azalan = [
      generalController.index - 1,
      generalController.index - 5,
      generalController.index - 3,
      generalController.index,
    ];
    List indexabcd_3_azalan = [
      generalController.index,
      generalController.index - 3,
      generalController.index - 5,
      generalController.index - 1,
    ];
    List indexabcd_4_azalan = [
      generalController.index - 1,
      generalController.index - 5,
      generalController.index,
      generalController.index - 3,
    ];

    var indexabcd;
    if (multipleChoiceQuestionController.kelime_listesi.length - index > 6) {
      if (generalController.index % 4 == 0) {
        indexabcd = indexabcd_1_artan;
        print("index abcd_1_artan yazdırılıyor ");
      } else if (generalController.index % 4 == 1) {
        indexabcd = indexabcd_2_artan;
        print("index abcd_2_artan yazdırılıyor ");
      } else if (generalController.index % 4 == 2) {
        indexabcd = indexabcd_3_artan;
        print("index abcd_3_artan yazdırılıyor ");
      } else if (generalController.index % 4 == 3) {
        indexabcd = indexabcd_4_artan;
        print("index abcd_4_artan yazdırılıyor ");
      }
    } else if (index > 7) {
      if (generalController.index % 4 == 0) {
        indexabcd = indexabcd_1_azalan;
        print("index abcd_1_azalan yazdırılıyor ");
      } else if (generalController.index % 4 == 1) {
        indexabcd = indexabcd_2_azalan;
        print("index abcd_2_azalan yazdırılıyor ");
      } else if (generalController.index % 4 == 2) {
        indexabcd = indexabcd_3_azalan;
        print("index abcd_3_azalan yazdırılıyor ");
      } else if (generalController.index % 4 == 3) {
        indexabcd = indexabcd_4_azalan;
        print("index abcd_4_azalan yazdırılıyor ");
      }
    }

    print("index Listabcd yazdırılıyor $indexabcd");
    var answerOptions = [
      multipleChoiceQuestionController.kelime_listesi[indexabcd[0]]["deutsch"],
      multipleChoiceQuestionController.kelime_listesi[indexabcd[1]]["deutsch"],
      multipleChoiceQuestionController.kelime_listesi[indexabcd[2]]["deutsch"],
      multipleChoiceQuestionController.kelime_listesi[indexabcd[3]]["deutsch"],
    ];

    print("answer options  $answerOptions");
    print("cevap verildi mi? ${multipleChoiceQuestionController.cevap_verildi_mi}");

    print("getx düzgün çalışsın diye yazdırılan sayı: ${generalController.sayi}"); //Bunu yapmazsak Getx düzgün kullanılmadı hatası veriyor.

    print("buildBody1 anındaki gc index ${generalController.index} ve mcindex ${multipleChoiceQuestionController.index}");
    print("mobile_screen_multiple_choice_question buildBody1 başladığı anda indexe atanan değer ${multipleChoiceQuestionController.index.value}");
    multipleChoiceQuestionController.dogru_cevap.value = multipleChoiceQuestionController.kelime_listesi[index]["deutsch"];
    var dogru_cevap = multipleChoiceQuestionController.dogru_cevap.value;
    var verilen_cevap;
    print("buildBody1 başladığı anda doğru cevaba atanan değer ${multipleChoiceQuestionController.dogru_cevap.value}");

    multipleChoiceQuestionController.answer_options_text_a.value = answerOptions[0];
    multipleChoiceQuestionController.answer_options_text_b.value = answerOptions[1];
    multipleChoiceQuestionController.answer_options_text_c.value = answerOptions[2];
    multipleChoiceQuestionController.answer_options_text_d.value = answerOptions[3];
    //multipleQuestionController.cevap_verildi_mi.value = false;
    var selectedValue;

    final items2 = List<String>.generate(multipleChoiceQuestionController.kelime_listesi.length, (counter) => "${counter + 1}");

    return SafeArea(
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [Colors.red, Colors.lightBlue],
          ),
        ),
        child: Center(
          child: multipleChoiceQuestionController.kelime_listesi.length< 20
              ? Text("There aren't enough words in your list (min 20)".tr)
              : Column(
                //mainAxisAlignment: MainAxisAlignment.center,
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  //Save the Question Button
                  Row(mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                    children: [
                      MaterialButton(
                          color: Colors.black,
                          child: Text(
                            "Save the question number".tr,
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            savedQuestionNumberStorage.write("savedQuestionNumberKey", index);
                            print("index değeri kaydedildikten sonraki değer ${savedQuestionNumberStorage.read("savedQuestionNumberKey")}");
                          }),

                      //Go to the Quesiton Button
                      MaterialButton(
                          color: Colors.black,
                          child: Text(
                            "Go to the saved question".tr,
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            if (savedQuestionNumberStorage.read("savedQuestionNumberKey") < generalController.word_list_of_choosen_bundesland.length) {
                              multipleChoiceQuestionController.index.value = savedQuestionNumberStorage.read("savedQuestionNumberKey");
                              generalController.index = savedQuestionNumberStorage.read("savedQuestionNumberKey");
                              print("kaydedilen index değeri ${savedQuestionNumberStorage.read("savedQuestionNumberKey")} ");
                              multipleChoiceQuestionController.cevap_verildi_mi.value = false;

                              multipleChoiceQuestionController.verilen_cevap.value = "";
                              multipleChoiceQuestionController.sonuc.value = "";



                              multipleChoiceQuestionController.make_question_options_white();
                              Get.to(MobileScreenMultipleChoiceQuestion());
                            } else {
                              print("kaydedilen soru numarası index aralığında değil");
                            }
                          }),
                    ],
                  ),
                  //hintext box

                  //Question Box
                  Container(
                    width: Get.width,
                    constraints: BoxConstraints(minHeight: 100),
                    margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white70,
                      border: Border.all(color: Colors.black),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: Text(
                        "${index + 1}. ${multipleChoiceQuestionController.kelime_listesi[index]["latin"]}"+" "+"means what in German".tr+"?",
                        textAlign: TextAlign.center,
                        softWrap: true,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),

                  //Answers Boxes
                  Column(
                    children: [
                      //Answer A Box
                      GestureDetector(
                        onTap: () {
                          if (multipleChoiceQuestionController.cevap_verildi_mi.value == false) {
                            verilen_cevap = answerOptions[0];
                            multipleChoiceQuestionController.cevap_verildi_mi.value = true;
                            if (verilen_cevap == dogru_cevap) {
                              multipleChoiceQuestionController.a_soru_secenek_rengi.value = Colors.green;
                              print("a seçeneği seçildi");
                              print("doğru cevaba atana değer: ${dogru_cevap}");
                              multipleChoiceQuestionController.a_dogru_cevap.value = true;

                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap ${multipleChoiceQuestionController.verilen_cevap.value}. Cevabınız doğru";

                              print(multipleChoiceQuestionController.sonuc.value);
                            } else {
                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap ${multipleChoiceQuestionController.verilen_cevap.value} ancak. Cevabınız yanlış. Doğru cevap ${multipleChoiceQuestionController.dogru_cevap.value}. ";
                              print(multipleChoiceQuestionController.sonuc.value);
                              multipleChoiceQuestionController.a_soru_secenek_rengi.value = Colors.red;
                            }
                          }
                          multipleChoiceQuestionController.answer_box_color_check();
                        },
                        child: Container(
                          constraints: BoxConstraints(minHeight: 50, minWidth: double.infinity, maxHeight: double.maxFinite),
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            color: multipleChoiceQuestionController.a_soru_secenek_rengi.value,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Text(
                            multipleChoiceQuestionController.answer_options_text_a.value,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      //Answer B Box
                      GestureDetector(
                        onTap: () {
                          if (multipleChoiceQuestionController.cevap_verildi_mi.value == false) {
                            multipleChoiceQuestionController.cevap_verildi_mi.value = true;
                            verilen_cevap = answerOptions[1];
                            if (verilen_cevap == dogru_cevap) {
                              print("b seçeneği seçildi");
                              print("doğru cevaba atana değer: ${dogru_cevap}");
                              multipleChoiceQuestionController.b_dogru_cevap.value = true;
                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap $verilen_cevap. Cevabınız doğru";
                              multipleChoiceQuestionController.b_soru_secenek_rengi.value = Colors.green;
                              print(multipleChoiceQuestionController.sonuc.value);
                            } else {
                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap $verilen_cevap ancak. Cevabınız yanlış. Doğru cevap $dogru_cevap. ";
                              print(multipleChoiceQuestionController.sonuc.value);
                              multipleChoiceQuestionController.b_soru_secenek_rengi.value = Colors.red;
                            }
                          }
                          multipleChoiceQuestionController.answer_box_color_check();
                        },
                        child: Container(
                          constraints: BoxConstraints(minHeight: 50, minWidth: double.infinity, maxHeight: double.maxFinite),
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            color: multipleChoiceQuestionController.b_soru_secenek_rengi.value,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Text(
                            multipleChoiceQuestionController.answer_options_text_b.value,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      //Answer C Box
                      GestureDetector(
                        onTap: () {
                          if (multipleChoiceQuestionController.cevap_verildi_mi.value == false) {
                            multipleChoiceQuestionController.cevap_verildi_mi.value = true;
                            verilen_cevap = answerOptions[2];
                            if (verilen_cevap == dogru_cevap) {
                              print("c seçeneği seçildi");
                              print("doğru cevaba atana değer: $dogru_cevap");
                              multipleChoiceQuestionController.c_dogru_cevap.value = true;
                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap $verilen_cevap. Cevabınız doğru";
                              multipleChoiceQuestionController.c_soru_secenek_rengi.value = Colors.green;
                              print(multipleChoiceQuestionController.sonuc.value);
                            } else {
                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap $verilen_cevap ancak. Cevabınız yanlış. Doğru cevap $dogru_cevap. ";
                              multipleChoiceQuestionController.c_soru_secenek_rengi.value = Colors.red;
                              print(multipleChoiceQuestionController.sonuc.value);
                            }
                          }
                          multipleChoiceQuestionController.answer_box_color_check();
                        },
                        child: Container(
                          constraints: BoxConstraints(minHeight: 50, minWidth: double.infinity, maxHeight: double.maxFinite),
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            color: multipleChoiceQuestionController.c_soru_secenek_rengi.value,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Text(
                            multipleChoiceQuestionController.answer_options_text_c.value,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                      //Answer D Box
                      GestureDetector(
                        onTap: () {
                          if (multipleChoiceQuestionController.cevap_verildi_mi.value == false) {
                            multipleChoiceQuestionController.cevap_verildi_mi.value = true;
                            verilen_cevap = answerOptions[3];
                            if (verilen_cevap == dogru_cevap) {
                              print("d seçeneği seçildi");
                              print("doğru cevaba atana değer: $dogru_cevap");
                              multipleChoiceQuestionController.d_dogru_cevap.value = true;
                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap $verilen_cevap. Cevabınız doğru";
                              multipleChoiceQuestionController.d_soru_secenek_rengi.value = Colors.green;
                              print(multipleChoiceQuestionController.sonuc.value);
                            } else {
                              multipleChoiceQuestionController.sonuc.value = "Verdiğiniz cevap $verilen_cevap ancak. Cevabınız yanlış. Doğru cevap $dogru_cevap";
                              print(multipleChoiceQuestionController.sonuc.value);
                              multipleChoiceQuestionController.d_soru_secenek_rengi.value = Colors.red;
                            }
                          }
                          multipleChoiceQuestionController.answer_box_color_check();
                        },
                        child: Container(
                          constraints: BoxConstraints(minHeight: 50, minWidth: double.infinity, maxHeight: double.maxFinite),
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            color: multipleChoiceQuestionController.d_soru_secenek_rengi.value,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Text(
                            multipleChoiceQuestionController.answer_options_text_d.value,
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                  //Previous and Next Questions Buttons
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      IconButton(
                        splashColor: Colors.blue,
                        iconSize: 50,
                        icon: Icon(
                          Icons.arrow_left,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          if (index == 0) {
                            Get.snackbar("Info!".tr, "This is the last question!".tr, snackPosition: SnackPosition.TOP, backgroundColor: Colors.red, colorText: Colors.white);
                          } else if (index > 0) {
                            multipleChoiceQuestionController.cevap_verildi_mi.value = false;
                            multipleChoiceQuestionController.index.value--;
                            generalController.index--;
                            multipleChoiceQuestionController.verilen_cevap.value = "";
                            multipleChoiceQuestionController.sonuc.value = "";
                            multipleChoiceQuestionController.a_dogru_cevap.value = false;
                            multipleChoiceQuestionController.make_question_options_white();
                            generalController.hinttext.value = "Choose".tr;
                            Get.to(MobileScreenMultipleChoiceQuestion());
                          }
                        },
                      ),
                      CustomDropdownButton2(
                        buttonWidth: 100,
                        buttonHeight: 40,
                        dropdownHeight: Get.height / 2,
                        hint: generalController.hinttext.value.tr,
                        hintAlignment: Alignment.center,
                        buttonDecoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        dropdownItems: items2,
                        value: selectedValue,
                        onChanged: (value) {
                          selectedValue = int.parse(value);

                          generalController.hinttext.value = selectedValue.toString();

                          generalController.index = selectedValue - 1; //Bunu kullanmazsak soru Değişmiyor.
                          multipleChoiceQuestionController.index.value = selectedValue - 1; // Bunu kullanmazsak seçenekler değişmiyor
                          if (index < multipleChoiceQuestionController.kelime_listesi.length - 1) {
                            multipleChoiceQuestionController.cevap_verildi_mi.value = false;

                            multipleChoiceQuestionController.verilen_cevap.value = "";
                            multipleChoiceQuestionController.sonuc.value = "";
                            multipleChoiceQuestionController.a_dogru_cevap.value = false;

                            multipleChoiceQuestionController.make_question_options_white();


                            Get.to(MobileScreenMultipleChoiceQuestion());
                          }
                        },
                      ),
                      IconButton(
                        splashColor: Colors.blue,
                        iconSize: 50,
                        icon: Icon(
                          Icons.arrow_right,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          if (index == multipleChoiceQuestionController.kelime_listesi.length - 1) {
                            Get.snackbar("Info!".tr, "This is the last question!".tr, snackPosition: SnackPosition.TOP, backgroundColor: Colors.red, colorText: Colors.white);
                          } else if (index < multipleChoiceQuestionController.kelime_listesi.length - 1) {
                            multipleChoiceQuestionController.cevap_verildi_mi.value = false;
                            generalController.hinttext.value = "Choose".tr;
                            multipleChoiceQuestionController.verilen_cevap.value = "";
                            multipleChoiceQuestionController.sonuc.value = "";
                            multipleChoiceQuestionController.a_dogru_cevap.value = false;
                            multipleChoiceQuestionController.make_question_options_white();
                            // multipleChocieQuestionController.kelime_listesi_cevap_haric = multipleChocieQuestionController.kelime_listesi.removeAt(index);
                            //multipleChocieQuestionController.kelime_listesi_cevap_haric.shuffle();
                            multipleChoiceQuestionController.index.value++;
                            generalController.index++;
                            Get.to(MobileScreenMultipleChoiceQuestion());
                          }
                        },
                      ),
                    ],
                  )
                ],
              ),
        ),
      ),
    );
  }
}
