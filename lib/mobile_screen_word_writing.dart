import 'package:fach_sprache_prufung/controllers/controller_get_storage.dart';
import 'package:fach_sprache_prufung/controllers/controller_word_writing.dart';
import 'package:fach_sprache_prufung/controllers/controller_general.dart';
import 'package:fach_sprache_prufung/mobile_screen_navigation_drawer.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'mobile_screen_home.dart';
import 'mobile_screen_working_options.dart';


final getStorageController = Get.put(GetStorageController());
final box = GetStorage();
final generalController = Get.put(GeneralController());
final wordWritingController = Get.put(WordWritingController());

class MobileScreenWordWriting extends StatelessWidget {
  MobileScreenWordWriting({Key key}) : super(key: key);

  var favorilistesi = getStorageController.favorilistesi;
  var liste = generalController.kelime_listesi_belirle();
  final tag = "mobile_screen_word_writing.dart";

  @override
  Widget build(BuildContext context) {
    print("build anındaki index ${wordWritingController.index.value}");

    return WillPopScope(
      onWillPop: () async {
        Get.to(() => MobileScreenNeumorphicWorkingOptions());
        word_writing_reset();
      },
      child: SafeArea(
        child: Scaffold(
          drawer: NavigationDrawer(),
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.black),
            brightness: Brightness.dark,
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            toolbarHeight: 60,
            title: Text(
              "Word Writing".tr,
              style: TextStyle(color: Colors.black),
            ),
            actions: [
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      Get.offAll(MobileScreenHome());
                      word_writing_reset();
                    },
                    child: Icon(Icons.home, color: Colors.black),
                  )),
            ],
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black54, width: 1),
                // borderRadius: BorderRadius.only(
                //   topLeft: Radius.circular(20),
                //   bottomRight: Radius.circular(20),
                // ),
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Color(0xFF4da7db),
                    Colors.white,
                  ],
                ),
              ),
            ),
          ),
          body: buildBody(),
        ),
      ),
    );
  }

  buildBody() {
    if (liste.length == 0) {
      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Colors.red, Colors.lightBlue],
            ),
          ),
          child: Center(child: Text("There is no word in your list".tr)),
        ),
      );
    } else {
      return Obx(
        () => buildBody1(),
      );
    }
  }

  buildBody1() {
    print("writing buildBody1 çalıştı");
    print("getx düzgün çalışsın diye yazdırılan sayı: ${generalController.sayi}");
    var index = wordWritingController.index.value;
    print("buildBody1 anındaki index: $index");
    final choosen_bundesland = generalController.choosen_bundesland;
    print("seçilen eyalet $choosen_bundesland olacak şekilde $tag sayfası çalıştı");
    var liste = generalController.kelime_listesi_belirle();
    print(liste);
    var item = liste[index];
    var list_length = liste.length;

    print("liste uzunluğu${list_length}");
    print("buildBody1 anındaki hintex ${wordWritingController.answer_box_hintext.value}");

    return SafeArea(
      child: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [Colors.red, Colors.lightBlue],
          ),
        ),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: FlipCard(
                    alignment: Alignment.center,
                    direction: FlipDirection.HORIZONTAL,
                    front: Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black, width: 2),
                            borderRadius: BorderRadius.circular(10),
                            color: wordWritingController.question_box_foreground_color.value,
                          ),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          width: 200,
                          height: 200,
                          child: Text("${item["deutsch"]}", textAlign: TextAlign.center,style:TextStyle(fontSize: 20)),
                        ),
                        Positioned(
                          child: GestureDetector(
                              onTap: () {
                                getStorageController.orneksayi.value++;

                                var orneksayi = getStorageController.orneksayi.value;
                                print(orneksayi);
                                favorilistesi = box.read("favoriler"); // null
                                if (favorilistesi == null) favorilistesi = [];
                                if (generalController.choosen_bundesland == "Favorite Words") {
                                  getStorageController.orneksayi.value++;

                                  if (favorilistesi == null) favorilistesi = [];
                                  favorilistesi.removeWhere((element) => element["deutsch"] == "${item["deutsch"]}");
                                  box.write("favoriler", favorilistesi);
                                  favorilistesi = box.read("favoriler");
                                  getStorageController.orneksayi.value++;
                                } else {
                                  favorilistesi.add({"deutsch": "${item["deutsch"]}", "latin": "${item["latin"]}"});
                                  box.write("favoriler", favorilistesi);
                                  favorilistesi = box.read("favoriler");
                                  getStorageController.orneksayi.value++;
                                }
                                generalController.choosen_bundesland == "Favorite Words" ? Get.offAll(() => MobileScreenWordWriting()) : Get.to(MobileScreenWordWriting());
                                Get.snackbar(
                                  "Info!".tr,
                                  generalController.choosen_bundesland == "Favorite Words" ? "${item["deutsch"]}"+" "+"has been deleted from the list".tr : "${item["deutsch"]}"+" "+"has been added to favorite words list".tr,
                                  icon: Icon(Icons.gpp_good, color: Colors.white),
                                  snackPosition: SnackPosition.BOTTOM,
                                  backgroundColor: Colors.black,
                                  borderRadius: 20,
                                  margin: EdgeInsets.all(15),
                                  colorText: Colors.white,
                                  duration: Duration(seconds: 1),
                                  isDismissible: true,
                                  dismissDirection: DismissDirection.horizontal,
                                  forwardAnimationCurve: Curves.easeOutBack,
                                );
                              },
                              child: Icon(generalController.choosen_bundesland == "Favorite Words" ? Icons.delete_forever : Icons.favorite, color: generalController.choosen_bundesland == "Favorite Words" ? Colors.black : Colors.red)),
                          top: 20,
                          right: 20,
                        ),
                      ],
                    ),
                    back: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 2),
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.blue,
                      ),
                      margin: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      width: 200,
                      height: 200,
                      child: Text("${item["latin"]}", textAlign: TextAlign.center,style:TextStyle(fontSize: 20)),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: TextField(
                    controller: wordWritingController.fieldText,
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: wordWritingController.answer_box_hintext.value.tr,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(color: Colors.black, width: 15),
                        )),
                    onSubmitted: (String answer_text) {
                      answer_text = answer_text.trim();
                      if ("${generalController.word_list_of_choosen_bundesland[index]["latin"]}" == answer_text) {
                        print("Girilen metin ${generalController.word_list_of_choosen_bundesland[index]["latin"]} ve cevabınız doğru");
                        wordWritingController.question_box_foreground_color.value = Colors.green;
                      } else {
                        wordWritingController.question_box_foreground_color.value = Colors.red;
                        print("cevabınız $answer_text ancak yanlış. Doğru cevap ${generalController.word_list_of_choosen_bundesland[index]["latin"]} olmalıydı");
                      }
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      splashColor: Colors.blue,
                      iconSize: 50,
                      icon: Icon(Icons.arrow_left, color: Colors.white),
                      onPressed: () {
                        print("önceki soru");
                        if (wordWritingController.index.value > 0) {
                          wordWritingController.index.value--;
                          word_writing_reset();
                          print("${wordWritingController.index.value}");
                          Get.to(MobileScreenWordWriting());
                        }
                      },
                    ),
                    IconButton(
                      splashColor: Colors.blue,
                      iconSize: 50,
                      icon: Icon(Icons.arrow_right, color: Colors.white),
                      onPressed: () {
                        print("sonraki soru");
                        print("liste uzunluğu ${list_length}");
                        if (wordWritingController.index.value < list_length - 1) {
                          wordWritingController.index.value++;
                          word_writing_reset();

                          Get.to(MobileScreenWordWriting());

                          print("index numarası ${wordWritingController.index.value}");
                        }
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

word_writing_reset() {
  wordWritingController.question_box_foreground_color.value = Colors.white54;
  wordWritingController.clearText();
}
