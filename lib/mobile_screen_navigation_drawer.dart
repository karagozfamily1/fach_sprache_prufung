import 'package:fach_sprache_prufung/controllers/controller_general.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:launch_review/launch_review.dart';

class NavigationDrawer extends StatelessWidget {
  NavigationDrawer({Key key}) : super(key: key);
  final generalController = Get.put(GeneralController());
  var locale = Locale("en", "US");

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          buildDrawHeader(),
          ExpansionTile(
            childrenPadding: EdgeInsets.all(20),
            leading: Icon(
              Icons.language,
              color: Colors.blueAccent,
            ),
            title: Text("Language".tr, style: TextStyle(color: Colors.black)),
            children: [
              SizedBox(height: 5),
              buildDrawerItem(
                text: "🇩🇪   " + "Deutsch".tr,
                textIconColor: Colors.black,
                tileColor: Colors.grey[300],
                onTap: () async {
                  locale = Locale("de", "DE");
                  Get.updateLocale(locale);
                  generalController.sayi.value++;
                  Get.back();
                  await Future.delayed(Duration(milliseconds: 300));
                },
              ),
              SizedBox(height: 5),
              buildDrawerItem(
                text: "🇹🇷   " + "Türkçe".tr,
                textIconColor: Colors.black,
                tileColor: Colors.grey[300],
                onTap: () async {
                  locale = Locale("tr", "TR");
                  Get.updateLocale(locale);
                  generalController.sayi.value++;
                  Get.back();
                  await Future.delayed(Duration(milliseconds: 300));
                },
              ),
              SizedBox(height: 5),
              buildDrawerItem(
                text: "🇺🇸   " + "English".tr,
                textIconColor: Colors.black,
                tileColor: Colors.grey[300],
                onTap: () async {
                  locale = Locale("en", "US");
                  Get.updateLocale(locale);
                  generalController.sayi.value++;
                  Get.back();
                  await Future.delayed(Duration(milliseconds: 300));
                },
              ),
              // SizedBox(height: 5),
              // buildDrawerItem(
              //   text: "🇸🇦   " + "Arabic",
              //   textIconColor: Colors.black,
              //   tileColor: Colors.grey[300],
              //   onTap: () async {
              //     locale = Locale("ar", "AR");
              //     Get.updateLocale(locale);
              //     generalController.sayi.value++;
              //     Get.back();
              //     await Future.delayed(Duration(milliseconds: 300));
              //   },
              // ),
            ],
          ),

          Divider(color: Colors.grey),
          buildDrawerItem(
            text: "Share This App".tr,
            icon: Icons.share,
            textIconColor: Colors.black,
            tileColor: Colors.white,
            onTap: () {
              LaunchReview.launch(
                androidAppId: "net.vidinli.fach_sprache_prufung&hl=DE",
                iOSAppId: "1633690849",
              );
            },
          ),
          Divider(color: Colors.grey),
          buildDrawerItem(
            text: "Rate This App".tr,
            icon: Icons.star_rate,
            iconColor: Colors.yellow,
            textIconColor: Colors.black,
            tileColor: Colors.white,
            onTap: () {
              LaunchReview.launch(
                androidAppId: "net.vidinli.fach_sprache_prufung&hl=DE",
                iOSAppId: "1633690849",
              );
              Get.back();
            },
          ),
        ],
      ),
    );
  }
}

buildDrawHeader() {
  return UserAccountsDrawerHeader(
    accountName: Text("Fach Sprache Prüfung".tr),
    accountEmail: Text("💌       info@vidinli.net"),
    currentAccountPicture: CircleAvatar(
      backgroundImage: AssetImage("assets/images/home_page_logo.png"),
    ),
    currentAccountPictureSize: Size.square(72),
  );
}

buildDrawerItem({
  @required String text,
  @required IconData icon,
  @required Color textIconColor,
  Color iconColor,
  Color tileColor,
  @required VoidCallback onTap,
}) {
  return ListTile(
    leading: Icon(icon, color: iconColor != null ? iconColor : textIconColor),
    title: Text(text, style: TextStyle(color: textIconColor)),
    tileColor: tileColor,
    onTap: onTap,
  );
}
