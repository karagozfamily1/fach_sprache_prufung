import 'package:fach_sprache_prufung/mobile_screen_working_options.dart';
import 'package:fach_sprache_prufung/mobile_screen_remembercard_deutsch_latin.dart';
import 'package:fach_sprache_prufung/mobile_screen_word_writing.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'controllers/controller_general.dart';
import 'mobile_screen_home.dart';
import 'mobile_screen_multiple_choice_question.dart';


class ButtonNeomorphism extends StatelessWidget {
  final String text;
  final Color textcolor;
  final TextAlign textAlign;

  //final Function onPressed; it didn't worked
  final Icon icon;
  final Function onTap;

  const ButtonNeomorphism({Key key, this.text, this.textAlign, this.textcolor, this.onTap, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: 300,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon,
            MaterialButton(
              padding: EdgeInsets.all(20),
              onPressed: () {},
              child: Text(
                text.tr,
                style: textcolor != null ? TextStyle(color: textcolor) : TextStyle(color: Colors.black),
              ),
            ),
          ],
        ),
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.shade900,
              offset: Offset(5, 5),
              blurRadius: 8,
              spreadRadius: 1,
            ),
            BoxShadow(
              color: Colors.white,
              offset: Offset(-5, -5),
              blurRadius: 8,
              spreadRadius: 1,
            ),
          ],
        ),
      ),
    );
  }
}

// AppBarCustom(appBarTitle) {
//   final generalController = Get.put(GeneralController());
//
//   return AppBar(
//     actions: [
//       PopupMenuButton(
//         icon: Icon(Icons.menu),
//         itemBuilder: (context) => [
//           PopupMenuItem<int>(
//             value: 0,
//             child: Text("Home".tr),
//           ),
//           PopupMenuItem<int>(
//             value: 1,
//             child: Text("Working Options".tr),
//           ),
//           PopupMenuItem<int>(
//             value: 2,
//             child: Text("Remember Card".tr),
//           ),
//           // PopupMenuItem<int>(
//           //   value: 3,
//           //   child: Text("Listening"),
//           // ),
//           PopupMenuItem<int>(
//             value: 4,
//             child: Text("Multiple Choice\nQuestions"),
//           ),
//           // PopupMenuItem<int>(
//           //   value: 5,
//           //   child: Text("Word Detail"),
//           // ),
//           PopupMenuItem<int>(
//             value: 6,
//             child: Text("Word Writing"),
//           ),
//         ],
//         onSelected: (item) => {
//           if (item == 0)
//             {
//               print(item),
//               Get.offAll(MobileScreenHome()),
//               generalController.index = 0,
//               generalController.dogru_cevap = "",
//               generalController.ikaz_metni.value = "",
//             },
//           if (item == 1)
//             {
//               print(item),
//               Get.offAll(MobileScreenNeumorphicWorkingOptions()),
//               generalController.index = 0,
//               generalController.dogru_cevap = "",
//               generalController.ikaz_metni.value = "",
//             },
//           if (item == 2)
//             {
//               print(item),
//               Get.offAll(MobileScreenRememberCardDeutschLatin()),
//               generalController.index = 0,
//               generalController.dogru_cevap = "",
//               generalController.ikaz_metni.value = "",
//             },
//           // if (item == 3) {print(item), Get.to(MobileScreenListening()), generalController.index = 0},
//           if (item == 4)
//             {
//               print(item),
//               Get.offAll(MobileScreenMultipleChoiceQuestion()),
//               generalController.index = 0,
//               generalController.dogru_cevap = "",
//               generalController.ikaz_metni.value = "",
//             },
//           //if (item == 5) {print(item), Get.to(MobileScreenWordDetail()),generalController.index=0},
//           if (item == 6)
//             {
//               print(item),
//               Get.offAll(MobileScreenWordWriting()),
//               generalController.index = 0,
//               generalController.dogru_cevap = "",
//               generalController.ikaz_metni.value = "",
//             },
//         },
//       )
//     ],
//     automaticallyImplyLeading: false,
//     title: Text(appBarTitle),
//     centerTitle: true,
//     // backgroundColor: generalController.guvez,
//   );
// }


