import 'package:confetti/confetti.dart';
import 'package:fach_sprache_prufung/mobile_screen_working_options.dart';
import 'package:fach_sprache_prufung/mobile_screen_navigation_drawer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:collection/collection.dart';
import 'package:get_storage/get_storage.dart';
import 'controllers/controller_get_storage.dart';
import 'controllers/controller_general.dart';
import 'package:fach_sprache_prufung/controllers/controller_general.dart';

RxList<dynamic> sublist1 = generalController.fill_sublist1();
RxList<dynamic> sublist2 = generalController.fill_sublist2();

final box = GetStorage();
final confettiController = ConfettiController(duration: Duration(seconds: 1));
final getStorageController = Get.put(GetStorageController());
var generalController = Get.put(GeneralController());
var renk = Colors.red;
Function deepEq = const DeepCollectionEquality().equals;

class MobileScreenWordMatch extends StatelessWidget {
  MobileScreenWordMatch({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("-----------------sayfa başladığında kelime listesi------------- ${generalController.word_list_of_choosen_bundesland}");
// print("----------------sayfa başladığında sublist1------------------ $sublist1"); //yorum satırına almazsak liste boş olunca sayfa burada hata verip yüklenmiyor.
//     print("----------------sayfa başladığında sublist2 ------------------------$sublist2"); //yorum satırına almazsak liste boş olunca sayfa burada hata verip yüklenmiyor.
    // print("sayfa başlatıldığında favori listesi uzunluğu ${liste.length} ve liste şöyle $liste");
    return Stack(alignment: Alignment.center, children: [
      WillPopScope(
        onWillPop: () => Get.offAll(() => MobileScreenNeumorphicWorkingOptions()), // Get.to ile yapınca bu sayfadan çıkıp geri gir yaptığında kırmızı ekran veriyor.
        child: SafeArea(
          child: Scaffold(
            drawer: NavigationDrawer(),
            floatingActionButton: FloatingActionButton.small(
              onPressed: () {
                if (sublist1.length != 0) {
                  print("list1 karşılaştırmaya giren hali $sublist1");
                  print("list2 karşılaştırmaya giren hali $sublist2");

                  if (deepEq(sublist1, sublist2)) {
                    confettiController.play();

                    Get.snackbar(
                      "Info!".tr,
                      "Your Answer is correct".tr,
                      icon: Icon(Icons.gpp_good, color: Colors.white),
                      snackPosition: SnackPosition.TOP,
                      backgroundColor: Colors.green,
                      borderRadius: 20,
                      margin: EdgeInsets.all(15),
                      colorText: Colors.white,
                      duration: Duration(seconds: 2),
                      isDismissible: true,
                      dismissDirection: DismissDirection.vertical,
                      forwardAnimationCurve: Curves.easeOutBack,
                    );
                  } else {
                    Get.snackbar(
                      "Info!".tr,
                      "Your Answer is incorrect".tr,
                      icon: Icon(Icons.gpp_good, color: Colors.white),
                      snackPosition: SnackPosition.TOP,
                      backgroundColor: Colors.red,
                      borderRadius: 20,
                      margin: EdgeInsets.all(15),
                      colorText: Colors.white,
                      duration: Duration(seconds: 2),
                      isDismissible: true,
                      dismissDirection: DismissDirection.vertical,
                      forwardAnimationCurve: Curves.easeOutBack,
                    );
                  }
                  // => true
                } else {
                  print("sublistler boş");
                }
              },
              child: Icon(Icons.done),
              backgroundColor: Colors.green,
            ),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              toolbarHeight: 60,
              title: Text("Word Match".tr, style: TextStyle(color: Colors.black)),
              centerTitle: true,
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black54, width: 1),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color(0xFF4da7db),
                      Colors.white,
                    ],
                  ),
                ),
              ),
              actions: [
                GestureDetector(
                  child: Icon(Icons.help_outline),
                  onTap: () {
                    AlertDialog alert = AlertDialog(
                      title:Text('How to Play'.tr+"?",textAlign: TextAlign.center),
                      content: Text('Long Press and reorder'.tr,textAlign: TextAlign.center),
                    );
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return alert;
                      },
                    );
                  },
                ),
                SizedBox(width: 10),
                GestureDetector(
                  child: Icon(Icons.keyboard_double_arrow_right, size: 35, color: Colors.black),
                  onTap: () {
                    Get.offAll(() => MobileScreenWordMatch()); // offAll olmazsa sayfa yenilenmiyor ve listeler değişmiyor.
                    print("---------------get off All çalıştı-----------------");
                    generalController.skip_count.value += 5;
                    sublist1 = generalController.fill_sublist1();
                    sublist2 = generalController.fill_sublist2();
                  },
                )
              ],
            ),
            body: buildBody(),
          ),
        ),
      ),
      ConfettiWidget(
        confettiController: confettiController,
        shouldLoop: false,
        blastDirectionality: BlastDirectionality.explosive,
        numberOfParticles: 50,
        minBlastForce: 10,
        maxBlastForce: 100,
        emissionFrequency: 0.4,
        gravity: 0.8,
      ),
    ]);
  }
}

buildBody() {
  var liste = generalController.kelime_listesi_belirle();
  print("liste uzunluğu ${liste.length} sayı uzunluğu ${generalController.skip_count.value + 5}");
  print("liste uzunluğu sıfır ama liste şu şekilde $liste");
  liste.length > generalController.skip_count.value + 5 ? print("liste büyük") : print("liste küçük");

  if (liste.length < generalController.skip_count.value + 5) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [Colors.red, Colors.lightBlue],
          ),
        ),
        child: Center(child: Text("There aren't enough words in your list (min 5)".tr)),
      ),
    );
  } else {
    sublist1 = generalController.fill_sublist1();
    sublist2 = generalController.fill_sublist2();
    return Obx(
      () => buildBody1(),
    );
  }
}

buildBody1() {
  return Container(
    decoration: BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.bottomLeft,
        end: Alignment.topRight,
        colors: [Colors.red, Colors.lightBlue],
      ),
    ),
    child: SafeArea(
      child: Row(
        //crossAxisAlignment: CrossAxisAlignment.stretch, //Tam sayfa olmasını sağlıyor
        children: [
          Expanded(
              child: Container(
            margin: EdgeInsets.all(5),
            child: Theme(
              data: ThemeData(canvasColor: Colors.transparent),
              child: ReorderableListView(
                onReorder: (oldIndex, newIndex) {
                  print("oldindex $oldIndex newindex $newIndex");
                  if (newIndex > oldIndex) newIndex--;
                  print("iften sonra bir azaltılan newindex $newIndex");
                  final item1 = sublist1.removeAt(oldIndex);
                  print("item silindikten sonraki liste1 $sublist1");
                  sublist1.insert(newIndex, item1);

                  print("item insert edildikten sonraki yeni liste1 sırası ${sublist1}");
                },
                children: [
                  for (final item1 in sublist1)
                    Padding(
                      key: ValueKey(item1),
                      padding: const EdgeInsets.all(5.0),
                      child: AspectRatio(
                        aspectRatio: 2,
                        child: Container(
                          key: ValueKey(item1),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white54,
                          ),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          child: SingleChildScrollView(child: Text(item1["deutsch"], textAlign: TextAlign.center)),
                        ),
                      ),
                    )
                ],
              ),
            ),
          )),
          Expanded(
              child: Container(
            margin: EdgeInsets.all(5),
            child: Theme(
              data: ThemeData(canvasColor: Colors.transparent),
              child: ReorderableListView(
                onReorder: (oldIndex, newIndex) {
                  print("oldindex $oldIndex newindex $newIndex");
                  if (newIndex > oldIndex) newIndex--;
                  print("iften sonra bir azaltılan newindex $newIndex");
                  final item2 = sublist2.removeAt(oldIndex);
                  print("item silindikten sonraki liste2 ${sublist2}");
                  sublist2.insert(newIndex, item2);

                  print("item insert edildikten sonraki yeni liste2 sırası ${sublist2}");
                },
                children: [
                  for (final item2 in sublist2)
                    Padding(
                      key: ValueKey(item2),
                      padding: const EdgeInsets.all(5.0),
                      child: AspectRatio(
                        aspectRatio: 2,
                        child: Container(
                          key: ValueKey(item2),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white54,
                          ),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          child: SingleChildScrollView(child: Text(item2["latin"], textAlign: TextAlign.center)),
                        ),
                      ),
                    )
                ],
              ),
            ),
          )),
        ],
      ),
    ),
  );
}

