import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:fach_sprache_prufung/mobile_screen_home.dart';
class MultipleQuestionController extends GetxController {
  var index = 0.obs;
  var dogru_cevap = "".obs;
  List kelime_listesi = generalController.word_list_of_choosen_bundesland;

  var avoid;
  var verilen_cevap = "".obs;
  var cevap_verildi_mi = false.obs;
  var sonuc = "Henüz cevap verilmedi".obs;
  var a_dogru_cevap = false.obs;
  var b_dogru_cevap = false.obs;
  var c_dogru_cevap = false.obs;
  var d_dogru_cevap = false.obs;
  var a_soru_secenek_rengi = Colors.white.obs;
  var b_soru_secenek_rengi = Colors.white.obs;
  var c_soru_secenek_rengi = Colors.white.obs;
  var d_soru_secenek_rengi = Colors.white.obs;
  var answer_options_text_a = "".obs;

  var answer_options_text_b = "".obs;
  var answer_options_text_c = "".obs;
  var answer_options_text_d = "".obs;
  var index_numbers_for_question = [];
  var cevap_secenekleri_listesi = [].obs;
  var cevap_secenekleri_listesi_ilk_eleman;
  var cevap_secenekleri_listesi_ikinci_eleman;
  List indexabcd_1_azalan = [generalController.index, generalController.index + 2, generalController.index + 4, generalController.index + 6];

  List indexabcd_2 = [generalController.index + 2, generalController.index + 4, generalController.index, generalController.index + 6];
  List indexabcd_3 = [generalController.index + 2, generalController.index + 4, generalController.index + 6, generalController.index];
  List indexabcd_4 = [generalController.index, generalController.index + 2, generalController.index + 4, generalController.index + 6];

  make_question_options_white() {
    a_soru_secenek_rengi.value = Colors.white;
    b_soru_secenek_rengi.value = Colors.white;
    c_soru_secenek_rengi.value = Colors.white;
    d_soru_secenek_rengi.value = Colors.white;
  }

  answer_box_color_check() {
    if (answer_options_text_a.value == dogru_cevap.value) {
      a_soru_secenek_rengi.value = Colors.green;
    }
    if (answer_options_text_b.value == dogru_cevap.value) {
      b_soru_secenek_rengi.value = Colors.green;
    }
    if (answer_options_text_c.value == dogru_cevap.value) {
      c_soru_secenek_rengi.value = Colors.green;
    }
    if (answer_options_text_d.value == dogru_cevap.value) {
      d_soru_secenek_rengi.value = Colors.green;
    }
  }
}
