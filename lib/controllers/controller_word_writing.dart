import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WordWritingController extends GetxController {
  final fieldText = TextEditingController();

  var question_box_foreground_color = Colors.white54.obs;
  var index = 0.obs;
  var answer_box_hintext = "Write your answer and press enter".obs;
  clearText() {
    fieldText.clear();
  }
}
