import 'package:fach_sprache_prufung/controllers/controller_get_storage.dart';
import 'package:fach_sprache_prufung/mobile_screen_navigation_drawer.dart';
import 'package:fach_sprache_prufung/mobile_screen_working_options.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/controller_general.dart';
import 'package:flutter/services.dart';

final generalController = Get.put(GeneralController());
final getStorageController = Get.put(GetStorageController());
final tag = "mobile_screen_home.dart";
final appBarTitle = "Home".tr;

class MobileScreenHome extends StatelessWidget {
  //const ({Key? key}) : super(key: key); Hata veriyor

  @override
  Widget build(BuildContext context) {
    //Programın sadece dikey konumda çalışması için alttaki kodlar yazılıyor. down yazan yeri silince yine aynı şekilde çalışıyor.
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Colors.red, Colors.lightBlue],
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            drawer: NavigationDrawer(),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              brightness: Brightness.dark,
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              toolbarHeight: 60,
              title: Text(
                appBarTitle.tr,
                style: TextStyle(color: Colors.black),
              ),
              centerTitle: true,
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black54, width: 2),
                  // borderRadius: BorderRadius.only(
                  //   topLeft: Radius.circular(20),
                  //   bottomRight: Radius.circular(20),
                  // ),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Colors.white,
                      Color(0xFF4da7db),
                    ],
                  ),
                ),
              ),
            ),
            body: Center(
              child: Column(
                children: [
                  //MaterialButton(onPressed: () => donustur(), child: Text("Word Match"), color: Colors.white),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Container(
                        child: Center(
                          //child: Image.asset("assets/images/home_page.png"),

                          child: Image.asset("assets/images/home_page_logo.png"),
                          //Image.network("https://cdn.pixabay.com/photo/2020/09/28/23/12/flags-5611288_960_720.png"),
                        ),
                        height: 200,
                        width: Get.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.transparent,
                        )),
                  ),
                  Expanded(
                    flex: 7,
                    child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemCount: generalController.bundeslander_has_questions.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            margin: EdgeInsets.all(10),
                            height: 50,
                            child: MaterialButton(
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                children: [
                                  //Image.network("https://cdn.pixabay.com/photo/2017/05/05/14/38/germany-2287230_960_720.png"),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 5),
                                    child: Image.network("https://cdn.pixabay.com/photo/2017/05/05/14/38/germany-2287230_960_720.png"),
                                    //child: Text("🇩🇪",style: TextStyle(fontSize: 40),),
                                  ),
                                  Text("${generalController.bundeslander_has_questions[index]}".tr,style: TextStyle(fontSize:18)),
                                ],
                              ),
                              color: Colors.white,
                              onPressed: () {
                                //Eyalet seçip o eyaletin apilerinin alındığı working options sayfasına gidiyor
                                generalController.choosen_bundesland = generalController.bundeslander_has_questions[index];
                                print("Seçilen eyalet ${generalController.choosen_bundesland}");

                                Get.offAll(() => MobileScreenNeumorphicWorkingOptions());
                              },
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
