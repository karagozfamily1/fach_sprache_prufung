import 'package:fach_sprache_prufung/mobile_screen_word_match.dart';
import 'package:fach_sprache_prufung/controllers/controller_general.dart';
import 'package:fach_sprache_prufung/mobile_screen__remember_card_latin_deutsch.dart';
import 'package:fach_sprache_prufung/mobile_screen_navigation_drawer.dart';
import 'package:fach_sprache_prufung/mobile_screen_word_writing.dart';
import 'package:fach_sprache_prufung/mobile_screen_multiple_choice_question.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'mobile_screen_home.dart';
import 'mobile_screen_remembercard_deutsch_latin.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class MobileScreenNeumorphicWorkingOptions extends StatelessWidget {
  MobileScreenNeumorphicWorkingOptions({Key key}) : super(key: key);
  final generalController = Get.put(GeneralController());
  final tag = "MobileScreenNeumorphicWorkingOptions";

  var locale = Locale("en", "US");

  @override
  Widget build(BuildContext context) {
    print("${generalController.choosen_bundesland} olarak kelime listesi yüklendi");

    print("$tag ekranı build çalıştı");
    generalController.index = 0;
    //print("build anındaki index ${generalController.index}");

    return WillPopScope(
      onWillPop: () => Get.to(() => MobileScreenHome()),
      child: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              colors: [Colors.white60, Colors.white],
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.white30,
            drawer: NavigationDrawer(),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              brightness: Brightness.dark,
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              toolbarHeight: 60,
              title: Text(
                "Working Options".tr,
                style: TextStyle(color: Colors.black)
              ),
              centerTitle: true,
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black54, width: 1),
                  // borderRadius: BorderRadius.only(
                  //   topLeft: Radius.circular(20),
                  //   bottomRight: Radius.circular(20),
                  // ),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color(0xFF4da7db),
                      Colors.white,
                    ],
                  ),
                ),
              ),
            ),
            body: buildBody(),
          ),
        ),
      ),
    );
  }

  buildBody() {
    print("$tag buildBody çalıştı");
    return Obx(
      () => buildBody1(),
    );
  }

  buildBody1() {
    generalController.skip_count.value=0;// bunu yapmazsak wordmatch sayfasındaki kelime listesi sıfırdan başlamıyor.
    final choosen_bundesland = generalController.choosen_bundesland;
     print("$tag buildBody1 çalıştı");
    print("seçilen eyalet $choosen_bundesland olacak şekilde $tag sayfası buildBody1 çalıştı");
    generalController.word_list_of_choosen_bundesland = generalController.kelime_listesi_belirle();



     print("${generalController.choosen_bundesland} eyaleti kelime listesi: ${generalController.word_list_of_choosen_bundesland}");
    print("getx düzgün çalışsın diye yazdırılan sayı: ${generalController.sayi}");

    return AnimationLimiter(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
                child: Center(
                  //child: Image.asset("assets/images/home_page.png"),

                  child: Image.asset("assets/images/home_page_logo.png"),
                  //Image.network("https://cdn.pixabay.com/photo/2020/09/28/23/12/flags-5611288_960_720.png"),
                ),
                height: 200,
                width: Get.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.transparent,
                )),
          ),
          Container(
            child: Text(
              "${generalController.choosen_bundesland} " + "State's words count is:".tr + " ${generalController.word_list_of_choosen_bundesland.length}",
            ),
          ),
          Expanded(
            flex: 7,
            child: GridView.builder(
              itemCount: generalController.working_options.length,
              padding: EdgeInsets.all(10).copyWith(top: 20),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                mainAxisSpacing: 5,
                crossAxisSpacing: 5,
                crossAxisCount: 1,
                childAspectRatio: 5,
              ),
              itemBuilder: (context, index) {
                return AnimationConfiguration.staggeredGrid(
                  duration: const Duration(milliseconds: 375),
                  columnCount: 2,
                  position: index,
                  child: ScaleAnimation(
                    child: FadeInAnimation(
                      duration: Duration(milliseconds: 300),
                      child: GestureDetector(
                        onTap: () {
                          if (index == 0) {
                            Get.offAll(() => MobileScreenMultipleChoiceQuestion());
                            generalController.index = 0;
                          }
                          if (index == 1) {
                            Get.offAll(() => MobileScreenWordWriting());
                            generalController.index = 0;
                            wordWritingController.index.value = 0;
                          }
                          if (index == 2) {
                            Get.offAll(() => MobileScreenRememberCardDeutschLatin());
                            generalController.index = 0;
                          }
                          if (index == 3) {
                            Get.offAll(() => MobileScreenRememberCardLatinDeutsch());
                            generalController.index = 0;
                          }
                          if (index == 4) {
                            Get.offAll(() => MobileScreenWordMatch());
                            generalController.index = 0;
                          }
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment(-1.0, -4.0),
                                end: Alignment(1.0, 4.0),
                                colors: [
                                  Color(0xff578392),
                                  Color(0xFF4da7db),
                                ],
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(35)),
                              boxShadow: [
                                BoxShadow(color: Color(0xFF4ca5d8), offset: Offset(5.0, 5.0), blurRadius: 15.0, spreadRadius: 1.0),
                                BoxShadow(color: Color(0xFF5ecdff), offset: Offset(-5.0, -5.0), blurRadius: 15.0, spreadRadius: 1.0),
                              ]),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          child: Text(
                            generalController.working_options[index].tr,
                            textAlign: TextAlign.center,style:TextStyle(fontSize: 18,fontWeight:FontWeight.bold)
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
