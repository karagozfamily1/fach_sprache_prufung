import 'package:fach_sprache_prufung/controllers/controller_get_storage.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'mobile_screen_home.dart';

final getStorageController = Get.put(GetStorageController());
var box = GetStorage();

class FavoriteWords extends StatelessWidget {
  FavoriteWords({Key key}) : super(key: key);
  var favorilistesi = box.read("favoriler");

  var item_global;

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () => Get.to(() => MobileScreenHome()),
      child: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Colors.red, Colors.lightBlue],
            ),
          ),
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              toolbarHeight: 60,
              title: Text(
                "Favorite Words".tr,
                style: TextStyle(color: Colors.black),
              ),
              centerTitle: true,
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black54, width: 2),
                  // borderRadius: BorderRadius.only(
                  //   topLeft: Radius.circular(20),
                  //   bottomRight: Radius.circular(20),
                  // ),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color(0xFF4da7db),
                      Colors.white,
                    ],
                  ),
                ),
              ),
              actions: [
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        Get.offAll(MobileScreenHome());
                      },
                      child: Icon(Icons.home, color: Colors.black),
                    )),
              ],
            ),
            body: buildBody(),
          ),
        ),
      ),
    );
  }

  buildBody() {
    return Obx(
      () => buildBody1(),
    );
  }

  buildBody1() {

    var orneksayi = getStorageController.orneksayi.value;
    print(orneksayi);

    favorilistesi = box.read("favoriler");

    if (favorilistesi == null) favorilistesi = [];
    print("buildBody1 başladığı anda favori listesi ${favorilistesi}");
    print("buildBody1 başladı ve box read yapıldıktan sonraki favori listesi ${favorilistesi}");

    return Center(
      child: Column(
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: MaterialButton(
                child: Text("Erase the list".tr),
                onPressed: () {
                  getStorageController.orneksayi.value++;
                  favorilistesi=[];

                  box.write("favoriler", favorilistesi);

                  Get.snackbar(
                                      "Info!".tr,
                                      "Favorite Words List has been erased".tr,
                                      icon: Icon(Icons.info_outline, color: Colors.white),
                                      snackPosition: SnackPosition.BOTTOM,
                                      backgroundColor: Colors.black,
                                      borderRadius: 20,
                                      margin: EdgeInsets.all(15),
                                      colorText: Colors.white,
                                      duration: Duration(seconds: 1),
                                      isDismissible: true,
                                      dismissDirection: DismissDirection.horizontal,
                                      forwardAnimationCurve: Curves.easeOutBack,
                                    );
                },
                color: Colors.red,
              ),
            ),
          ),
          Expanded(
            child: GridView.builder(
              itemCount: favorilistesi.length,
              padding: EdgeInsets.all(10).copyWith(top: 20),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(mainAxisSpacing: 5, crossAxisSpacing: 5, crossAxisCount: 2, childAspectRatio: 1),
              itemBuilder: (context, index) {
                var item = favorilistesi[index];
                item_global = item;
                return AnimationConfiguration.staggeredGrid(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  columnCount: 2,
                  child: ScaleAnimation(
                    child: FadeInAnimation(
                      child: FlipCard(
                        alignment: Alignment.center,
                        direction: FlipDirection.HORIZONTAL,
                        front: Stack(
                          fit: StackFit.expand,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white54,
                              ),
                              margin: EdgeInsets.all(10),
                              alignment: Alignment.center,
                              width: 50,
                              height: 50,
                              child: Text("${item["deutsch"]}", textAlign: TextAlign.center),
                            ),
                            Positioned(
                              child: GestureDetector(
                                  onTap: () {
                                    getStorageController.orneksayi.value++;

                                    if (favorilistesi == null) favorilistesi = [];
                                    favorilistesi.removeWhere((element) => element["deutsch"] == "${item["deutsch"]}");
                                    box.write("favoriler", favorilistesi);
                                    favorilistesi = box.read("favoriler");
                                    Get.snackbar(
                                      "Info!".tr,
                                      "${item["deutsch"]}"+" "+"has been deleted from the list".tr,
                                      icon: Icon(Icons.info_outline, color: Colors.white),
                                      snackPosition: SnackPosition.BOTTOM,
                                      backgroundColor: Colors.black,
                                      borderRadius: 20,
                                      margin: EdgeInsets.all(15),
                                      colorText: Colors.white,
                                      duration: Duration(seconds: 1),
                                      isDismissible: true,
                                      dismissDirection: DismissDirection.horizontal,
                                      forwardAnimationCurve: Curves.easeOutBack,
                                    );
                                  },
                                  child: Icon(Icons.delete_forever, color: Colors.black)),
                              top: 20,
                              right: 20,
                            ),
                          ],
                        ),
                        back: Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.blue,
                          ),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          width: 50,
                          height: 50,
                          child: Text("${item["latin"]}", textAlign: TextAlign.center),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
