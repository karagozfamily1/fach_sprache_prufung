import 'package:fach_sprache_prufung/mobile_screen_navigation_drawer.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'controllers/controller_get_storage.dart';
import 'controllers/controller_general.dart';
import 'mobile_screen_home.dart';
import 'mobile_screen_working_options.dart';

final generalController = Get.put(GeneralController());
final getStorageController = Get.put(GetStorageController());
final box = GetStorage();
final tag = "mobile_screen_remember_card_deutscht_latin.dart";

class MobileScreenRememberCardDeutschLatin extends StatelessWidget {
  var favorilistesi = getStorageController.favorilistesi;
  var liste = generalController.kelime_listesi_belirle();

  @override
  Widget build(BuildContext context) {
    final choosen_bundesland = generalController.choosen_bundesland;
    print("seçilen eyalet $choosen_bundesland olacak şekilde $tag sayfası çalıştı");

    print(liste);
    return WillPopScope(
      onWillPop: () => Get.to(() => MobileScreenNeumorphicWorkingOptions()),
      child: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Colors.red, Colors.lightBlue],
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            drawer: NavigationDrawer(),
            appBar: AppBar(
              iconTheme: IconThemeData(color: Colors.black),
              toolbarHeight: 60,
              title: Text(
                "Remember Card German-Latin".tr,
                style: TextStyle(color: Colors.black),
              ),
              actions: [
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () => Get.offAll(() => MobileScreenHome()),
                      child: Icon(Icons.home, color: Colors.black),
                    )),
              ],
              centerTitle: true,
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black54, width: 2),
                  // borderRadius: BorderRadius.only(
                  //   topLeft: Radius.circular(20),
                  //   bottomRight: Radius.circular(20),
                  // ),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color(0xFF4da7db),
                      Colors.white,
                    ],
                  ),
                ),
              ),
            ),
            body: buildBody(),
          ),
        ),
      ),
    );
  }

  buildBody() {
    if (liste.length == 0) {
      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Colors.red, Colors.lightBlue],
            ),
          ),
          child: Center(child: Text("There is no word in your list".tr)),
        ),
      );
    } else {
      return Obx(
        () => buildBody1(),
      );
    }
  }

  buildBody1() {
    var orneksayi = getStorageController.orneksayi.value;
    print(orneksayi);
    favorilistesi = box.read("favoriler");
    if (favorilistesi == null) favorilistesi = [];
    print("buildBody1 başladığı anda favori listesi ${favorilistesi}");
    print("buildBody1 başladı ve box read yapıldıktan sonraki favori listesi ${favorilistesi}");
    print(generalController.sayi);
    return Column(
      children: [
        // Eğer liste boş değilse ve favori kelimeler seçilmişse buton gözükecek.
        Visibility(
          visible: favorilistesi.length != 0 && generalController.choosen_bundesland == "Favorite Words" ? true : false,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: MaterialButton(
              child: Text("Erase the list".tr),
              onPressed: () {
                getStorageController.orneksayi.value++;
                favorilistesi = [];

                box.write("favoriler", favorilistesi);
                liste = favorilistesi;

                Get.snackbar(
                  "Info!".tr,
                  "Favorite Words List has been erased".tr,
                  icon: Icon(Icons.info_outline, color: Colors.white),
                  snackPosition: SnackPosition.BOTTOM,
                  backgroundColor: Colors.black,
                  borderRadius: 20,
                  margin: EdgeInsets.all(15),
                  colorText: Colors.white,
                  duration: Duration(seconds: 1),
                  isDismissible: true,
                  dismissDirection: DismissDirection.horizontal,
                  forwardAnimationCurve: Curves.easeOutBack,
                );
              },
              color: Colors.red,
            ),
          ),
        ),
        Expanded(
          child: AnimationLimiter(
            child: GridView.builder(
              itemCount: liste.length,
              padding: EdgeInsets.all(10).copyWith(top: 20),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(mainAxisSpacing: 5, crossAxisSpacing: 5, crossAxisCount: 2, childAspectRatio: 1),
              itemBuilder: (context, index) {
                var item = liste[index];
                return AnimationConfiguration.staggeredGrid(
                  position: index,
                  duration: const Duration(milliseconds: 375),
                  columnCount: 2,
                  child: ScaleAnimation(
                    child: FadeInAnimation(
                      child: FlipCard(
                        //flipOnTouch: false,
                        alignment: Alignment.center,
                        direction: FlipDirection.HORIZONTAL,
                        front: Stack(
                          fit: StackFit.expand,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white54,
                              ),
                              margin: EdgeInsets.all(10),
                              alignment: Alignment.center,
                              width: 50,
                              height: 50,
                              child: Text("${item["deutsch"]}", textAlign: TextAlign.center,style:TextStyle(fontSize: 20)),
                            ),
                            Positioned(
                              child: GestureDetector(
                                  onTap: () {
                                    getStorageController.orneksayi.value++;

                                    var orneksayi = getStorageController.orneksayi.value;
                                    print(orneksayi);
                                    favorilistesi = box.read("favoriler"); // null
                                    if (favorilistesi == null) favorilistesi = [];
                                    if (generalController.choosen_bundesland == "Favorite Words") {
                                      getStorageController.orneksayi.value++;

                                      if (favorilistesi == null) favorilistesi = [];
                                      favorilistesi.removeWhere((element) => element["deutsch"] == "${item["deutsch"]}");
                                      box.write("favoriler", favorilistesi);
                                      favorilistesi = box.read("favoriler");
                                      getStorageController.orneksayi.value++;
                                    } else {
                                      favorilistesi.add({"deutsch": "${item["deutsch"]}", "latin": "${item["latin"]}"});
                                      box.write("favoriler", favorilistesi);
                                      favorilistesi = box.read("favoriler");
                                      getStorageController.orneksayi.value++;
                                    }
                                    Get.to(() => MobileScreenRememberCardDeutschLatin());

                                    Get.snackbar(
                                      "Info!".tr,
                                      generalController.choosen_bundesland == "Favorite Words" ? "${item["deutsch"]}"+" "+"has been deleted from the list".tr : "${item["deutsch"]}"+" "+"has been added to favorite words list".tr,
                                      icon: Icon(Icons.gpp_good, color: Colors.white),
                                      snackPosition: SnackPosition.BOTTOM,
                                      backgroundColor: Colors.black,
                                      borderRadius: 20,
                                      margin: EdgeInsets.all(15),
                                      colorText: Colors.white,
                                      duration: Duration(seconds: 2),
                                      isDismissible: true,
                                      dismissDirection: DismissDirection.horizontal,
                                      forwardAnimationCurve: Curves.easeOutBack,
                                    );
                                  },
                                  child: Icon(generalController.choosen_bundesland == "Favorite Words" ? Icons.delete_forever : Icons.favorite, color: generalController.choosen_bundesland == "Favorite Words" ? Colors.black : Colors.red)),
                              top: 20,
                              right: 20,
                            ),
                          ],
                        ),
                        back: Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.blue,
                          ),
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          width: 50,
                          height: 50,
                          //child: Text("${item["latin"]} \n Tipp: ${item["tipp"]}", textAlign: TextAlign.center), Remember Cardın arka yüzüne tipp eklemek istersek
                          child: Text("${item["latin"]}", textAlign: TextAlign.center,style:TextStyle(fontSize: 20)),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
